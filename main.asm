%define BUFFER_SIZE 256

section .rodata ; read-only
    overflow_message: db "The string is larger than the specified buffer (max. 255 characters)", 0
    not_found_message: db "The entered string was not found", 0

section .text
%include "colon.inc"
%include "words.inc"

extern exit
extern print_string
extern string_length
extern print_newline
extern string_equals
extern read_word
extern print_error

extern find_word

global _start

section .text

; Читает строку размером не более 255 символов в буфер с `stdin`.
; Пытается найти вхождение в словаре; 
; если оно найдено, распечатывает в `stdout` значение по этому ключу.
; Иначе выдает сообщение об ошибке.
; Не забудьте, что сообщения об ошибках нужно выводить в `stderr`.
_start:	
	sub rsp, BUFFER_SIZE	    	; Space for stdin
	mov rdi, rsp
	mov rsi, BUFFER_SIZE
	call read_word
	test rax, rax
	jz .overflow

	mov rdi, rsp
	mov rsi, link_to_next_el
	call find_word
	add rsp, BUFFER_SIZE            ; Restore rsp	
	test rax, rax
	jz .not_found

    mov rdi, rax
    add rdi, 8			; dq = 8 bytes
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi				; skip null-terminator
    call print_string
    call print_newline
	call exit

.overflow:
    add rsp, BUFFER_SIZE            ; Restore rsp
	mov rdi, overflow_message
	call print_error
	call print_newline
	call exit

.not_found:
	mov rdi, not_found_message
	call print_error
	call print_newline
	call exit
