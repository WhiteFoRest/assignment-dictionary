%define link_to_next_el 0 ; только для первого раза

; %1 - Ключ (в кавычках)
; %2 - Имя метки, по которой будет находиться значение
%macro colon 2

    %ifstr %1
        %ifid %2
            %%next_el: dq link_to_next_el
            db %1, 0
            %2:
        %else
            %error "Метка не является валидным идентификатором"
        %endif
    %else
        %error "Не определен ключ"
    %endif

    %define link_to_next_el %%next_el

%endmacro